#include <math.h>
#include <uWS/uWS.h>
#include <chrono>
#include <iostream>
#include <thread>
#include <vector>
#include "Eigen-3.3/Eigen/Core"
#include "Eigen-3.3/Eigen/QR"
#include "MPC.h"
#include "json.hpp"

using namespace std;

// for convenience
using json = nlohmann::json;

// For converting back and forth between radians and degrees.
constexpr double pi() { return M_PI; }
double deg2rad(double x) { return x * pi() / 180; }
double rad2deg(double x) { return x * 180 / pi(); }

// Checks if the SocketIO event has JSON data.
// If there is data the JSON object in string format will be returned,
// else the empty string "" will be returned.
string hasData(string s) {
  auto found_null = s.find("null");
  auto b1 = s.find_first_of("[");
  auto b2 = s.rfind("}]");
  if (found_null != string::npos) 
  {
    return "";
  } else if (b1 != string::npos && b2 != string::npos) 
  {
    return s.substr(b1, b2 - b1 + 2);
  }
  return "";
}

// Evaluate a polynomial.
double polyeval(Eigen::VectorXd coeffs, double x) 
{
  double result = 0.0;
  for (unsigned int i = 0; i < coeffs.size(); i++) 
  {
    result += coeffs[i] * pow(x, i);
  }
  return result;
}

// Fit a polynomial.
// Adapted from
// https://github.com/JuliaMath/Polynomials.jl/blob/master/src/Polynomials.jl#L676-L716
Eigen::VectorXd polyfit(Eigen::VectorXd xvals, Eigen::VectorXd yvals, int order) 
{
  assert(xvals.size() == yvals.size());
  assert(order >= 1 && order <= xvals.size() - 1);
  Eigen::MatrixXd A(xvals.size(), order + 1);

  for (unsigned int i = 0; i < xvals.size(); i++) 
  {
    A(i, 0) = 1.0;
  }

  for (unsigned int j = 0; j < xvals.size(); j++)
  {
    for (int i = 0; i < order; i++)
	{
      A(j, i + 1) = A(j, i) * xvals(j);
    }
  }

  auto Q = A.householderQr();
  auto result = Q.solve(yvals);
  return result;
}

// Transform the waypoints from map coordinates to vehicle coordinates
void transform_to_vehicle_coord(const Eigen::VectorXd& xvals_m, const Eigen::VectorXd& yvals_m,
						        Eigen::VectorXd& xvals_v, Eigen::VectorXd& yvals_v, 
							    double psi_m, double px_m, double py_m)
{
	assert(xvals_m.size() == yvals_m.size());
	assert(xvals_v.size() == yvals_v.size());
	double x_diff = 0;
	double y_diff = 0;

	for (unsigned int i = 0; i < xvals_m.size(); ++i)
	{
		x_diff = xvals_m[i] - px_m;
		y_diff = yvals_m[i] - py_m;
		/* 
		 * Note if psi is positive we rotate counter-clockwise, or turn left. 
		 * In the simulator however, a positive value implies a right turn and 
		 * a negative value implies a left turn.
		 * Map co-ordinate to vehicle co-ordinate is anticlockwise 
		 * rotation by psi degrees
		 * The vehicle co-ordinates assumes clockwise rotation. 
		 * The transformation equations assume anticlockwise rotation.
		 * Hence, the psi is replaced with -psi in the anticlockwise 
		 * equations to get clockwise equations.
		 */
		xvals_v[i] = (cos(-psi_m) * x_diff) - (sin(-psi_m) * y_diff);
		yvals_v[i] = (sin(-psi_m) * x_diff) + (cos(-psi_m) * y_diff);
	}
}

double delay_s = 0.1;
int main() 
{
  uWS::Hub h;

  // creates MPC object
  MPC mpc;

  h.onMessage([&mpc](uWS::WebSocket<uWS::SERVER> ws, char *data, size_t length, uWS::OpCode opCode) 
  {
    // "42" at the start of the message means there's a websocket message event.
    // The 4 signifies a websocket message
    // The 2 signifies a websocket event
	  if (length && length > 2 && data[0] == '4' && data[1] == '2')
	  {
		auto s = hasData(std::string(data));
		if (s != "") 
		{
		    auto j = json::parse(s);

		    std::string event = j[0].get<std::string>();
			if (event == "telemetry") 
			{
			  // j[1] is the data JSON object
			  // receives current values of state variables x, y, \psi, v, actuators \delta, a, and 
			  // a vectors of 6 points for each x and $y$ coordinates of waypoints in global map coordinates 
			  // from the simulator
			  vector<double> ptsx_m = j[1]["ptsx"];
			  vector<double> ptsy_m = j[1]["ptsy"];
			  double px_m = j[1]["x"];
			  double py_m = j[1]["y"];
			  double psi_m = j[1]["psi"];
			  double v = j[1]["speed"];
			  double a = j[1]["throttle"];
			  double delta = j[1]["steering_angle"];

			  // Processing delay
			  Eigen::VectorXd current_state_m(4); 
			  current_state_m << px_m, py_m, psi_m, v;
			  Eigen::VectorXd actuators(2); 
			  actuators << delta, a;

			  // Map co-ordinates - Apply global kinematic model to calculate the next state based 
			  // on the current state, actuators, and delay
			  Eigen::VectorXd next_state_m = mpc.global_kinematic(current_state_m, actuators, delay_s);

			  // Convert state values from map co-ordinates to vehicle co-ordinates
			  // Values at the origin
			  double px_v = 0;
			  double py_v = 0;
			  double psi_v = 0;

			  // fit a polynomial to the above x and y coordinates
			  Eigen::VectorXd xvals_m = Eigen::Map<Eigen::VectorXd, Eigen::Unaligned>(ptsx_m.data(), ptsx_m.size());
			  Eigen::VectorXd yvals_m = Eigen::Map<Eigen::VectorXd, Eigen::Unaligned>(ptsy_m.data(), ptsy_m.size());

			  Eigen::VectorXd xvals_v(ptsx_m.size());
			  Eigen::VectorXd yvals_v(ptsy_m.size());

			  // transforms waypoint vectors from map coordinates to vehicle coordinates
			  transform_to_vehicle_coord(xvals_m, yvals_m, xvals_v, yvals_v, next_state_m(2), next_state_m(0), next_state_m(1));

			  // fits 3rd order polynomial to the transformed waypoints to calculate the fit coefficients
			  int fit_order = 3;
			  auto coeffs = polyfit(xvals_v, yvals_v, fit_order);

			  Eigen::VectorXd state_v(6);

			  // calculate the cross track error
			  double cte_v = polyeval(coeffs, px_v) - py_v;
			  // calculate the orientation error
			  double epsi_v = psi_v - atan(coeffs[1]);

			  // setup initial state 
			  state_v << px_v, py_v, psi_v, next_state_m(3), cte_v, epsi_v;

			  // passes in the initial state and fitted coefficients (reference trajectory)
			  mpc_results res = mpc.solve(state_v, coeffs); // outputs values for the next time step based on current state and fitted coeffs
			  			  
			  // Extract steering angle and throttle from the MPC results			  
			  double steer_value_v = res.delta;
			  double throttle_value = res.a;

			  cout << "Steering angle: " << steer_value_v << endl;

			  json msgJson;
			  /* 
			   *  The steering angle value is divided by deg2rad(25) so that the values are between [-1,1]
			   *  By default, the MPC algorithm returns steering angle values in the 
			   *  range [-deg2rad(25), deg2rad(25] because of the constraint set in the algorithm. 
			   *  The throttle value returned is in the range [-1,1]. 
			   */
			  msgJson["steering_angle"] = steer_value_v / deg2rad(25); 
			  msgJson["throttle"] = throttle_value;

			  //Display the MPC predicted trajectory 
			  vector<double> mpc_x_vals_v = res.xvals;
			  vector<double> mpc_y_vals_v = res.yvals;

			  //.. add (x,y) points to list here, points are in reference to the vehicle's coordinate system
			  // the points in the simulator are connected by a Green line

			  msgJson["mpc_x"] = mpc_x_vals_v;
			  msgJson["mpc_y"] = mpc_y_vals_v;

			  //Display the waypoints/reference line
			  vector<double> next_x_vals_v;
			  vector<double> next_y_vals_v;

			  for (unsigned int i=0; i < xvals_v.size(); ++i)
			  { 
				  next_x_vals_v.push_back(xvals_v[i]);
				  next_y_vals_v.push_back(yvals_v[i]);
			  }

			  //.. add (x,y) points to list here, points are in reference to the vehicle's coordinate system
			  // the points in the simulator are connected by a Yellow line

			  msgJson["next_x"] = next_x_vals_v;
			  msgJson["next_y"] = next_y_vals_v;

			  auto msg = "42[\"steer\"," + msgJson.dump() + "]";
			  std::cout << msg << std::endl;
			  // Latency
			  // The purpose is to mimic real driving conditions where
			  // the car does not actuate the commands instantly.
			  //
			  // Feel free to play around with this value but should be to drive
			  // around the track with 100ms latency.
			  this_thread::sleep_for(chrono::milliseconds(static_cast<long>(delay_s*1000.0)));
			  ws.send(msg.data(), msg.length(), uWS::OpCode::TEXT);
        }
      } 
	  else 
	  {
        // Manual driving
        std::string msg = "42[\"manual\",{}]";
        ws.send(msg.data(), msg.length(), uWS::OpCode::TEXT);
      }
    }
  });

  // We don't need this since we're not using HTTP but if it's removed the
  // program
  // doesn't compile :-(
  // We don't need this since we're not using HTTP but if it's removed the program
  // doesn't compile :-(
  h.onHttpRequest([](uWS::HttpResponse *res, uWS::HttpRequest req, char *data, size_t, size_t) 
  {
	  const std::string s = "<h1>Hello world!</h1>";
	  if (req.getUrl().valueLength == 1)
	  {
		  res->end(s.data(), s.length());
	  }
	  else
	  {
		  // i guess this should be done more gracefully?
		  res->end(nullptr, 0);
	  }
  });

  h.onConnection([&h](uWS::WebSocket<uWS::SERVER> ws, uWS::HttpRequest req) 
  {
	  std::cout << "Connected!!!" << std::endl;
  });

  h.onDisconnection([&h](uWS::WebSocket<uWS::SERVER> ws, int code, char *message, size_t length) 
  {
	  ws.close();
	  std::cout << "Disconnected" << std::endl;
  });

  int port = 4567;
  if (h.listen(port))
  {
	  std::cout << "Listening to port " << port << std::endl;
  }
  else
  {
	  std::cerr << "Failed to listen to port" << std::endl;
	  return -1;
  }
  h.run();
}
