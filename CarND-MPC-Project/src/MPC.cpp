#include "MPC.h"
#include <cppad/cppad.hpp>
#include <cppad/ipopt/solve.hpp>
#include "Eigen-3.3/Eigen/Core"

using CppAD::AD;

// TODO: Set the timestep length and duration
size_t N = 8;
double dt = 0.08;

double ref_v = 80;

// This value assumes the model presented in the classroom is used.
//
// It was obtained by measuring the radius formed by running the vehicle in the
// simulator around in a circle with a constant steering angle and velocity on a
// flat terrain.
//
// Lf was tuned until the the radius formed by the simulating the model
// presented in the classroom matched the previous radius.
//
// This is the length from front to CoG that has a similar radius.
const double Lf = 2.67;

// The solver takes all the state variables and actuator
// variables in a single vector. Thus, we should to establish
// when one variable starts and another ends to make our life easier.
size_t x_start = 0;
size_t y_start = x_start + N;
size_t psi_start = y_start + N;
size_t v_start = psi_start + N;
size_t cte_start = v_start + N;
size_t epsi_start = cte_start + N;
size_t delta_start = epsi_start + N;
size_t a_start = delta_start + N - 1;

class FG_eval 
{
 public:
  // Fitted polynomial coefficients
  Eigen::VectorXd coeffs;
  FG_eval(Eigen::VectorXd coeffs) { this->coeffs = coeffs; }

  typedef CPPAD_TESTVECTOR(AD<double>) ADvector;
  // `fg` is a vector containing the cost and constraints.
  // `vars` is a vector containing the variable values (state & actuators).
  void operator()(ADvector& fg, const ADvector& vars)
  {
	  /* Cost definition */
	  // The cost is stored in the first element of `fg`.
	  // Any additions to the cost should be added to `fg[0]`.
	  fg[0] = 0;

	  // Reference State Cost
	  for (unsigned int t = 0; t < N; t++)
	  {
		  fg[0] += 1800 * CppAD::pow(vars[cte_start + t], 2);
		  fg[0] += 1800 * CppAD::pow(vars[epsi_start + t], 2);
		  fg[0] += 10 * CppAD::pow(vars[v_start + t] - ref_v, 2);
	  }
	  // Minimize the use of actuators.
	  for (unsigned int t = 0; t < N - 1; t++)
	  {
		  fg[0] += 100 * CppAD::pow(vars[delta_start + t], 2);
		  fg[0] += 10 * CppAD::pow(vars[a_start + t], 2);
		  fg[0] += 600 * CppAD::pow(vars[delta_start + t] * vars[v_start + t], 2);
	  }
	  // Minimize the value gap between sequential actuations.
	  for (unsigned int t = 0; t < N - 2; t++)
	  {
		  fg[0] += 48 * CppAD::pow(vars[delta_start + t + 1] - vars[delta_start + t], 2); // multiplier for smooth steering angle transitions
		  fg[0] += 48 * CppAD::pow(vars[a_start + t + 1] - vars[a_start + t], 2); // multiplier for smooth throttle transitions
	  }

	  /* Setup Constraints */
	  // In this section we'll setup the model constraints.

	  // Initial constraints
	  //
	  // We add 1 to each of the starting indices due to cost being located at index 0 of `fg`.
	  // This bumps up the position of all the other values.
	  fg[1 + x_start] = vars[x_start];
	  fg[1 + y_start] = vars[y_start];
	  fg[1 + psi_start] = vars[psi_start];
	  fg[1 + v_start] = vars[v_start];
	  fg[1 + cte_start] = vars[cte_start];
	  fg[1 + epsi_start] = vars[epsi_start];

	  // The rest of the constraints
	  // Note that we start the loop at t = 1, because the values at t = 0 
	  // are set to our initial state - those values are not calculated by the solver.
	  for (unsigned int t = 1; t < N; t++)
	  {
		  // The state at time t+1 
		  AD<double> x1 = vars[x_start + t];
		  AD<double> y1 = vars[y_start + t];
		  AD<double> psi1 = vars[psi_start + t];
		  AD<double> v1 = vars[v_start + t];
		  AD<double> cte1 = vars[cte_start + t];
		  AD<double> epsi1 = vars[epsi_start + t];

		  // The state at time t 
		  AD<double> x0 = vars[x_start + t - 1];
		  AD<double> y0 = vars[y_start + t - 1];
		  AD<double> psi0 = vars[psi_start + t - 1];
		  AD<double> v0 = vars[v_start + t - 1];
		  AD<double> cte0 = vars[cte_start + t - 1];
		  AD<double> epsi0 = vars[epsi_start + t - 1];

		  // Only consider the actuation at time t
		  AD<double> delta0 = vars[delta_start + t - 1];
		  AD<double> a0 = vars[a_start + t - 1];

		  // Fit model order = 3
		  AD<double> f0 = coeffs[0] + (coeffs[1] * x0) + (coeffs[2] * CppAD::pow(x0,2)) + (coeffs[3] * CppAD::pow(x0, 3));

		  // Arc tangent of derivative of function f0 with respect to x0
		  AD<double> psides0 = CppAD::atan(coeffs[1] + (2 * coeffs[2] * x0) + (3 * coeffs[3] * CppAD::pow(x0, 2)));

		  // The idea here is to constraint each value to be 0.
		  //
		  // NOTE: The use of `AD<double>` and use of `CppAD` so that CppAD can compute derivatives and pass
		  // these to the solver.

		  // Setup the model constraints
		  fg[1 + x_start + t] = x1 - (x0 + v0 * CppAD::cos(psi0) * dt);
		  fg[1 + y_start + t] = y1 - (y0 + v0 * CppAD::sin(psi0) * dt);
		  fg[1 + v_start + t] = v1 - (v0 + a0 * dt);
		  fg[1 + cte_start + t] = cte1 - ((f0 - y0) + v0 * CppAD::sin(epsi0) * dt);
		   /* 
		     Note if psi is positive we rotate counter-clockwise, or turn left. 
		     In the simulator however, a positive value implies a right turn and 
			 a negative value implies a left turn. 
			 This is accomodated in the update equation by having a '-' instead of '+'.
		   */
		  fg[1 + psi_start + t] = psi1 - (psi0 - (v0 / Lf) * delta0 * dt);
		  fg[1 + epsi_start + t] = epsi1 - ((psi0 - psides0) - (v0 / Lf) * delta0 * dt);
	  }
  }
};


//
// MPC class definition implementation.
//
MPC::MPC() {}
MPC::~MPC() {}

mpc_results MPC::solve(const Eigen::VectorXd& x0, const Eigen::VectorXd& coeffs)
{
	typedef CPPAD_TESTVECTOR(double) Dvector;

	double x = x0[0];
	double y = x0[1];
	double psi = x0[2];
	double v = x0[3];
	double cte = x0[4];
	double epsi = x0[5];

	// number of independent variables
	// N timesteps == N - 1 actuations
	size_t n_vars = N * 6 + (N - 1) * 2; // 6 state variables and 2 control (or actuator) variables
	// Number of constraints
	size_t n_constraints = N * 6;

	/*
	 * We are carrying three Dvectors of size n_vars: vars[], vars_lowerbound[], vars_upperbound[]
	 * vars[] is initialized to 0s except for the first time step where in each variable is set to
	 * its initial value coming from input state vector x0
	 * vars_lowerbound[] is set to -1.0e19 for all the state variables.
	 * For actuators, the delta value is set to -0.436332 rad (-25 deg), and
	 * the acceleration value is set to -1.0 (full brake)
	 * vars_upperbound[] is set to 1.0e19 for all the state variables.
	 * For actuators, the delta value is set to 0.436332 rad (25 deg), and
	 * the acceleration value is set to 1.0 (full throttle).
	 */

	 // Initial value of the independent variables.
	 // Should be 0 except for the initial values.
	Dvector vars(n_vars);
	for (unsigned int i = 0; i < n_vars; i++)
	{
		vars[i] = 0.0;
	}
	// Set the initial variable values
	vars[x_start] = x;
	vars[y_start] = y;
	vars[psi_start] = psi;
	vars[v_start] = v;
	vars[cte_start] = cte;
	vars[epsi_start] = epsi;

	// Lower and upper limits for x
	Dvector vars_lowerbound(n_vars);
	Dvector vars_upperbound(n_vars);

	// Set all non-actuators upper and lowerlimits
	// to the max negative and positive values.
	for (unsigned int i = 0; i < delta_start; i++)
	{
		vars_lowerbound[i] = -1.0e19;
		vars_upperbound[i] = 1.0e19;
	}

	// The upper and lower limits of delta are set to -25 and 25
	// degrees (values in radians).
	// NOTE: Feel free to change this to something else.
	for (unsigned int i = delta_start; i < a_start; i++)
	{
		vars_lowerbound[i] = -0.436332;
		vars_upperbound[i] = 0.436332;
	}

	// Acceleration/decceleration upper and lower limits.
	// NOTE: Feel free to change this to something else.
	for (unsigned int i = a_start; i < n_vars; i++)
	{
		vars_lowerbound[i] = -1.0;
		vars_upperbound[i] = 1.0;
	}

	/*
	 * We are carrying two Dvectors of size n_constraints: constraints_lowerbound[], constraints_upperbound[]
	 * Both Dvectors are set to 0s except for the first time step where in each variable is set to
	 * its initial value coming from input state vector x0
	 */
	 // Lower and upper limits for constraints
	 // All of these should be 0 except the initial
	 // state indices.
	Dvector constraints_lowerbound(n_constraints);
	Dvector constraints_upperbound(n_constraints);
	for (unsigned int i = 0; i < n_constraints; i++)
	{
		constraints_lowerbound[i] = 0;
		constraints_upperbound[i] = 0;
	}
	constraints_lowerbound[x_start] = x;
	constraints_lowerbound[y_start] = y;
	constraints_lowerbound[psi_start] = psi;
	constraints_lowerbound[v_start] = v;
	constraints_lowerbound[cte_start] = cte;
	constraints_lowerbound[epsi_start] = epsi;

	constraints_upperbound[x_start] = x;
	constraints_upperbound[y_start] = y;
	constraints_upperbound[psi_start] = psi;
	constraints_upperbound[v_start] = v;
	constraints_upperbound[cte_start] = cte;
	constraints_upperbound[epsi_start] = epsi;

	// Object that computes objective and constraints
	FG_eval fg_eval(coeffs); // coeffs is the polynomial coefficients obtained by fitting to waypoints x and y

	// options for IPOPT solver
	std::string options;
	options += "Integer print_level  0\n";
	// NOTE: Setting sparse to true allows the solver to take advantage
	// of sparse routines, this makes the computation MUCH FASTER. If you
	// can uncomment 1 of these and see if it makes a difference or not but
	// if you uncomment both the computation time should go up in orders of
	// magnitude.
	options += "Sparse  true        forward\n";
	options += "Sparse  true        reverse\n";
	// NOTE: Currently the solver has a maximum time limit of 0.5 seconds.
	// Change this as you see fit.
	options += "Numeric max_cpu_time          0.5\n";

	// place to return solution
	CppAD::ipopt::solve_result<Dvector> solution; // solution contains the output of the solver

	// solve the problem
	CppAD::ipopt::solve<Dvector, FG_eval>( options, vars, vars_lowerbound, vars_upperbound, 
		                                   constraints_lowerbound, constraints_upperbound, fg_eval, solution);

	// Check some of the solution values
	bool ok = true;
	ok &= solution.status == CppAD::ipopt::solve_result<Dvector>::success;

	mpc_results res;
	res.xvals.resize(N - 1);
	res.yvals.resize(N - 1);
	res.psi.resize(N - 1);
	res.v.resize(N - 1);
	res.cte.resize(N - 1);
	res.epsi.resize(N - 1);

	res.cost = solution.obj_value;
	res.delta = solution.x[delta_start];
	res.a = solution.x[a_start];
	for (unsigned int i = 0; i < N - 1; ++i)
	{
		res.xvals.push_back(solution.x[x_start + 1 + i]);
		res.yvals.push_back(solution.x[y_start + 1 + i]);
		res.psi.push_back(solution.x[psi_start + 1 + i]);
		res.v.push_back(solution.x[v_start + 1 + i]);
		res.cte.push_back(solution.x[cte_start + 1 + i]);
		res.epsi.push_back(solution.x[epsi_start + 1 + i]);
	}	

	return res;
}

// Return the next state.
//
// NOTE: state is [x, y, psi, v]
// NOTE: actuators is [delta, a]
Eigen::VectorXd MPC::global_kinematic(const Eigen::VectorXd& current_state, const Eigen::VectorXd& actuators, double dt)
{
	// Create a new vector for the next state.
	Eigen::VectorXd next_state(current_state.size());

	auto x = current_state(0);
	auto y = current_state(1);
	auto psi = current_state(2);
	auto v = current_state(3);

	auto delta = actuators(0);
	auto a = actuators(1);

	// Recall the equations for the model:
	// x_[t+1] = x[t] + v[t] * cos(psi[t]) * dt
	// y_[t+1] = y[t] + v[t] * sin(psi[t]) * dt
	// psi_[t+1] = psi[t] + v[t] / Lf * delta[t] * dt
	// v_[t+1] = v[t] + a[t] * dt
	next_state(0) = x + v * cos(psi) * dt;
	next_state(1) = y + v * sin(psi) * dt;
	next_state(2) = psi - (v / Lf) * delta * dt;
	next_state(3) = v + a * dt;
	/*
	Note if psi is positive we rotate counter-clockwise, or turn left.
	In the simulator however, a positive value implies a right turn and
	a negative value implies a left turn.
	This is accomodated in the update equation by having a '-' instead of '+'.
	// psi_[t+1] = psi[t] - v[t] / Lf * delta[t] * dt
	*/

	return next_state;
}

