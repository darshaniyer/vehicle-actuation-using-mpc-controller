#ifndef MPC_H
#define MPC_H

#include <vector>
#include "Eigen-3.3/Eigen/Core"

using namespace std;

struct mpc_results
{
	vector<double> xvals;
	vector<double> yvals;
	vector<double> psi;
	vector<double> v;
	vector<double> cte;
	vector<double> epsi;
	double delta;
	double a;
	double cost;
};

class MPC 
{
 public:
  MPC();

  virtual ~MPC();

  // Solve the model given an initial state and polynomial coefficients.
  // Return the first actuations.
  mpc_results solve(const Eigen::VectorXd& x0, const Eigen::VectorXd& coeffs);

  // Apply global kinematic model to calculate the next state from current state, actuators, and delay dt
  Eigen::VectorXd global_kinematic(const Eigen::VectorXd& current_state, const Eigen::VectorXd& actuators, double dt);
};

#endif /* MPC_H */
