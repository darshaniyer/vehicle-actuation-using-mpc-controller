# Motion predictive controller for driving car around race track

Autonomous vehicle (AV) system architecture starts with the perception system, which estimates the state of the surrounding environment including landmarks, vehicles, and pedestrians. The localization block compares the model to the map to figure out where the vehicle is. The path planning block charts a trajectory using the environment model, the map, and vehicle location. Finally, the control loop applies the actuators in the form of steering, brake, and throttle to actuate the AV on the stipulated trajectory. Typically, the path planning block passes in the reference trajectory to the control block as a polynomial, such as third-degree polynomial as they fit most roads. 

When we as humans turn through an intersection, we use our intuition and experience to determine how hard to steer, when to accelerate, and when to apply brakes. AVs use control algorithms, often called controllers, to actuate the vehicle on a desired path. One of the most common and fundamental controllers is the proportional-integral-differental (PID) controller. The problem with PID controllers is that it will calculate the error with respect to the present state, but the actuation will be performed when the vehicle is in a future (and likely different state). This is called latency, which can sometimes lead to instability. The PID controller could try to compute a control input based on a future error, which, without a vehicle model, is highly unlikely to be accurate. 

In this project, we use a controller called motion predictive controller (MPC) that can adapt quite well to the latency issue by explicity modeling the latency in the system. The goal of this project is to implement a MPC in C++ to maneuver the vehicle around the simulator track. 

## Repository

The original Udacity project instructions can be found [here](https://github.com/udacity/CarND-MPC-Project).

## Code base

The project was implemented in Docker environment.

Start the term2 simulator. Select "Project 5: MPC Controller".

In the docker prompt, run following commands
1) git clone https://github.com/udacity/CarND-MPC-Project.git
2) mv CarND-MPC-Project MPC
3) cd MPC
4) Copy files main.cpp, MPC.h, and MPC.cpp from CarND-MPC-Project\src folder into MPC\src.
5) mkdir build && cd build && cmake .. && make
6) ./mpc

Please note that due to some bug in the simulator, sometimes the car may get derailed. In that case, run the command "./mpc" once again. It should work fine.

## Detailed writeup

Detailed report can be found in [_MPC_writeup.md_](MPC_writeup.md).

## Solution video

![](./videos/MPC_demo.mp4)

## Acknowledgments

I would like to thank Udacity for giving me this opportunity to work on an awesome project. I would like to offer thanks to Saurabh Sohoni as my approach to coordinate transformation, hyperparameter tuning, and latency handling were inspired by his work. Special thanks to the author of the [post](https://discussions.udacity.com/t/getting-started-with-docker-and-windows-for-the-ekf-project-a-guide/320236) which helped me setup the docker environment. 

