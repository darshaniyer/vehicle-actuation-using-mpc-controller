
# Motion predictive controller for driving car around race track 

Autonomous vehicle (AV) system architecture starts with the perception system, which estimates the state of the surrounding environment (environment model) including landmarks, vehicles, and pedestrians. The localization block compares the environment model to the map to figure out where the vehicle is. The path planning block charts a trajectory using the environment model, the map, and vehicle location. Finally, the control loop applies the actuators in the form of steering, brake, and throttle to actuate the AV on the stipulated trajectory. Typically, the path planning block passes in the reference trajectory to the control block as a polynomial, such as third-degree polynomial as they fit most roads. 

When we as humans turn through an intersection, we use our intuition and experience to determine how hard to steer, when to accelerate, and when to apply brakes. AVs use control algorithms, often called controllers, to actuate the vehicle on a desired path. One of the most common and fundamental controllers is the proportional-integral-differental (PID) controller. The problem with PID controllers is that it will calculate the error with respect to the present state, but the actuation will be performed when the vehicle is in a future (and likely different state). This is called latency, which can sometimes lead to instability. The PID controller could try to compute a control input based on a future error, which, without a vehicle model, is highly unlikely to be accurate. 

In this project, we use a controller called motion predictive controller (MPC) that can adapt quite well to the latency issue by explicity modeling the latency in the system. The goal of this project is to implement MPC in C++ to maneuver the vehicle around the simulator track. 

## MPC theory 

One approach to designing a good controller is to first model the vehicles dynamics and constraints. That way, we can analyze and tune the controller more effectively. 

### Vehicle models 

Vehicle models mathematically describe how the vehicle moves. Typically, more realistic models are more complex and challenging to deal with. In general, there are two kinds of vehicle models - kinematic and dynamic. 

#### Kinematic models 

Kinematic models are simpler models that ignore many elements like gravity, tire forces, and mass, but often perform well and are easy to work with. This simplification reduces the accuracy of the models, but makes them more tractable. At low and medium speeds, kinematic models often approximate the actual vehicle dynamics. 

#### Dynamic models 

On the other hand, dynamic models aim to embody the actual vehicle dynamics as closely as possible, by encompassing tire forces, longitudinal and lateral forces, inertia, gravity, air resistance, drag, mass, and geometry of the vehicle. Advanced dynamic models even take internal vehicle forces into account - for example, how responsive the chassis suspension is.

In this project, we will use kinematic model based on bicycle motion model. 

####  Building a kinematic model 

The state of the vehicle in motion is described by its position coordinates $x$ and $y$, its orientation $\psi$, and its velocity $v$. We want to derive a model that captures how a state evolves over time and how actuator inputs allow us to change the vehicle state. Most cars have three actuators - steering wheel, throttle pedal, and brake pedal. For simplicity, we consider throttle and brake pedals as a single actuator, with negative values signifying braking and positive values signifying throttle. This reduces the vehicle to two actuator inputs - $[\delta, a]$.

The following is the state transition model for state = $[x, y, \psi, v]$, actuator inputs = $[\delta, a]$, and time elapse $dt$:

$x_{t+1} = x_{t} + v_{t}.cos(\psi).dt$

$y_{t+1} = y_{t} + v_{t}.sin(\psi).dt$

$\psi_{t+1} = \psi_{t} + \frac{v_{t}}{L_{f}}.\delta_{t}.dt$

$v_{t+1} = v_{t} + a.dt$

$L_{f}$ measures the distance between the center of mass of the vehicle and its front axle. The larger the vehicle, the slower the turn rate. Since, we turn quicker at higher speeds than at lower speeds, $v$ is included in the $\psi$ update equation.

#### Errors 

A controller actuates the vehicle to follow the reference trajectory within a set of design requirements, of which an important one is to minimize the error between the reference trajectory and the actual path of the vehicle. We can minimize this error by predicting the actual path of the vehicle using the kinematic model, and then adjusting the control inputs over time to minimize the difference between the predicted and the reference trajectory. 

We can capture how the errors we are interested in change over time by deriving our kinematic model around these errors as our new state vector $[x, y, \psi, v, cte, e\psi]$. Let us assume our vehicle is traveling down a straight road and the longitudinal direction is the same as the x-axis. We can express the error between the center of the road and vehicle's position as the cross-trek error (CTE). The CTE at time $t+1$ is defined as

$cte_{t+1} = cte_{t} + v_{t}.sin(e\psi).dt$

The term $v_{t}.sin(e\psi).dt$ signifies the change in error caused by the vehicle's movement.

In this case, $cte_{t}$ can be expressed as the difference between the reference line and current vehicle position $y$. Assuming the reference line is a 3rd order polynomial $f(x_{t})$, CTE at the current time is defined as

$cte_{t} = y_{t} - f(x_{t})$, and 

$f(x) = c_{0} + c_{1}.x + c_{2}.x^{2} + c_{3}.x^{3}$

The update rule for the orientation error $e\psi$ is defined as

$e\psi_{t+1} = e\psi_{t} + \frac{v_{t}}{L_{f}}.\delta_{t}.dt$, where

$e\psi_{t} = \psi_{t} - \psi desired_{t}$, where

$\psi desired_{t}$ can be calculated as the tangential angle of the polynomial $f$ evaluated at $x_{t}$ and is given by $arctan(f'(x_{t}))$, where $f'$ is the derivative of the polynomial. The term $\frac{v_{t}}{L_{f}}.\delta_{t}.dt$ signifies the change in error caused by vehicle's movement.

Figure 1 illustrates CTE and $e\psi$.

[image1]: ./figures/CTE_and_orientation_error.jpg "CTE and orientation error"
![alt text][image1]
**Figure 1 CTE and orientation error. The dashed white line is the CTE.**

#### Actuator constraints 

In real vehicles, actuators are limited by the design of the vehicle and the fundamental physics. For example, a vehicle cannot have a steering angle of $90^{o}$. Hence, it does not make sense to consider these kinds of actuator inputs. Such a model is called nonholonomic model because the vehicle cannot move in arbitrary directions as it is limited by steering angle constraints. We can solve this problem by setting lower and upper bounds for the actuators such as $[-25^{o},25^{o}]$ for steering angle and $[-1,1]$ for acceleration. In practice, the bounds should mimic the actual vehicle as closely as possible.

### Optimization problem 

The vehicle model along with the actuator constraints form the foundations of MPC. What we have so far is the model that describes the future trajectory of the vehicle. In order to develop an optimal controller, we need to define a cost function that captures the error between the model-predicted trajectory and the reference trajectory, which we try to minimize. Hence, the name model predictive controller. 

Thus, MPC reframes the task of following a trajectory as an optimization problem, for which the solution is optimal trajectory.
As shown in the Figure 2, it involves simulating different actuator inputs, predicting the resulting trajectories, and selecting the trajectory with the minimum cost. Imagine we know our current state and the reference trajectory we want to follow. We optimize our actuator inputs at each step in time in order to minimize the cost of our predicted trajectory. Once we have found the lowest cost trajectory, we implement the optimal set of actuator commands, and throw away the rest of the trajectories we calculated. Instead of using the old trajectory we predicted in the last time step, we take our new state and use that to calculate the new optimal trajectory. In that sense, we are constantly calculating actuator inputs over a future horizon. And that is why this approach is also called receding horizon control (RHC). The model being only approximate, we do not predict the entire trajectory in the first pass. Once we perform our actuation commands, our trajectory might not be exactly the same the trajectory we predicted. So, it is crucial that we constantly re-evaluate to find the optimal actuations.

[image2]: ./figures/MPC_paths.jpg "MPC paths"
![alt text][image2]
**Figure 2 MPC paths.**

#### Cost function

A good start to the cost function is to think of the error that you would like to minimize. For example, measuring the offset from the center of the lane, where the center of the lane can be called the reference, or the desired state. We previously captured two errors in our state vector: $cte$ and $e\psi$. Ideally, both of these errors would be 0 - there would be no difference between the actual vehicle position and heading and the desired position and heading. Our cost should be a function of how far these errors are from 0.

Here’s one such example of how to increment our cost at each time step, $t$, over our desired time horizon (represented by total number of desired timesteps, $N$, below) - we want to minimize the total error accumulated over this time horizon:

* error = 0
* for N time steps:
    * error = error + $cte_{t}^{2}$
    * error = error + e$psi_{t}^{2}$


If the goal is to move from point A to point B, then coming to a halt in the middle of the reference trajectory is a problem. A simple solution is to set a reference velocity $v_{ref}$, and use cost function to penalize the vehicle for not maintaining that $v_{ref}$ as in

* for N time steps:
   * error = error + $(v_{t}-v_{ref})^{2}$
   
$v_{ref}$ also determines how fast the car could travel around the track.

The cost function is not limited to the state variables. We can also include the control inputs (as well as their combination with state variables), which would allow us to penalize the magnitude of the control inputs as well as their change rate. If we want to change lanes, for example, we would have a large CTE and we would want to penalize turning the vehicle very sharply, which would yield a smoother lane change. We could add control input as

* for N time steps:
   * error = error + $\delta_{t}^{2}$

To ensure smoother transitions in acceleration, we would add

* for N time steps:
   * error = error + $a_{t}^{2}$
   
We still need to capture the change rate of control input to add some temporal smoothness, by adding an additional term in the cost function that captures the difference between the next control input state and the current one as follows:

* for N-1 time steps:
   * error = error + $(\delta_{t+1}-\delta_{t})^{2}$
   * error = error + $(a_{t+1}-a_{t})^{2}$
  
We could also add a combination of state variable and control input, for example, $v$ and $\delta$ to ensure smooth control in the turns at higher speeds.

* for N time steps:
   * error = error + $(v_{t}*\delta_{t})^{2}$

This helps minimize speeds at higher steering angles and minimize steering angles at higher speeds. This ensures that the car takes smooth turns by reducing the speed while it reached maximum possible speed on straight ahead path.

To ensure smoother drive over a trajectory, different scaling factors (>1) are added to different parts of the cost function, which are sets of hyperparameters that need to be determined as part of the optimization process. The scaling factors influence the solver into keeping sequential values together.

#### Length and duration 

The prediction horizon is the duration over which future predictions are made. We will refer to this as $T$, which is a product of number of timesteps $N$ in the horizon, and time elapse between actuations $dt$. $N$, $dt$, and $T$ are hyperparameters we will need to tune for each MPC we build. $T$ should be few seconds at the most. Beyond that horizon, the environment will change enough that it will not make sense to predict any further into the future.

The goal of the MPC is to optimize control inputs $[\delta,a]$ by tuning them over the horizon until a low cost vector of control inputs is found. The length of this vector is determined by $N$:

$[\delta_{1}, a_{1}, ..., \delta_{N-1}, a_{N-1}]$

Thus, $N$ determines the number of variables optimized by the MPC and is the major driver of the computational cost.

MPC attempts to approximate a continuous reference trajectory by means of discrete paths between actuations. Larger values of $dt$ result in less frequent actuations, which makes it harder to accurately approximate a continuous reference trajectory. This is sometimes called the "discretization error". 

A good approach to setting $N$, $dt$, and $T$ is to first determine a reasonable range for $T$, and then tune $dt$ and $N$ appropriately, keeping the effect of each in mind. 

#### Latency issue 

In a real car, an actuation command will not execute instantly, i.e., there will be a delay as the command propagates through the system. A realistic delay might be on the order of 100ms. This problem is called latency, and is a difficult challenge for some controllers like PID to overcome. But MPC can adapt quite well because we can model this latency in the system. This latency could easily be modeled by a simple dynamic system and is incorporated into the vehicle model. One approach would be running a simulator using the vehicle model starting from the current state for the duration of the latency. The resulting state from the simulation is the new initial state for the MPC. Thus, MPC can deal with latency much more effectively, by explicitly taking it into account, than a PID controller.

### Putting it all together 

As stated above, MPC uses an optimizer to find the control inputs that minimize the cost function. We actually only execute the very first set of control inputs, which brings the vehicle to a new state, and then, repeat the process.

#### Overall steps 

[image3]: ./figures/MPC_overall_steps.jpg "MPC overall steps"
![alt text][image3]
**Figure 3 MPC overall steps.**

As depicted in Figure 3, 

1. We setup everything required for MPC loop which consist of defining the duration $T$ of the trajectory, by choosing $N$ and $dt$
2. We fit the polynomial to the waypoints to get the reference trajectory
3. We deal with latency issue by calculating a new initial state from the current initial state using the vehicle model
4. We calculate the initial CTE and e$\psi$ values
5. We define the vehicle model and the constraints such as: $\delta$ $\epsilon$ $[-25^{o}, 25^{o}]$, a $\epsilon$ $[-1, 1]$
6. We define the cost function as explained above
7. With the setup complete, we begin the state feedback loop. We pass the new initial state to MPC, and then call the optimization solver, which uses the new initial state, the model constraints, and the cost function to return a vector of optimal control inputs. We apply the first set of control inputs to the vehicle, and repeat the loop.



## Implementation details

The MPC is implemented in C++. In order to solve the optimization problem, the following libraries are very critical.

### Ipopt library 

Ipopt is the solver tool we will be using to optimize the control input. It is able to find locally optimal values, while keeping the constraints set directly to the actuators and the constraints defined by the vehicle model. Ipopt requires that we give Jacobians and Hessians directly as it does not compute these, for which we use a library called CppAD.

### CppAD 

CppAD is a library used for automatic differentiation. In order to use CppAD effectively, we have to use its `type` instead of regular `double` or `std::vector` types. Additionally, math functions must be called from CppAD. Luckily, most elementary math operations are overloaded. So calling $*,+,-,/$ will work as intended as long as it called on CppAD<`double`> instead of double.
    
### File structure 

Following are the project files in the *src* folder:

* *main.cpp* - 
  * main() - 
    - creates MPC object
    - receives current values of state variables $x, y, \psi, v$, actuators $\delta, a$,  and a vectors of 6 points for each $x$ and $y$ coordinates of waypoints in global map coordinates from the simulator
    - applies global kinematic model (using global_kinematic() in MPC object) to calculate the next state based on the current state, actuators, and delay 
    - transforms waypoint vectors from map coordinates to vehicle coordinates using transform_to_vehicle_coord()
    - fits 3rd order polynomial to the transformed waypoints to calculate the fit coefficients using the polyfit()
    - calculates the CTE and e$\psi$ using the fitted coefficients using the polyval()
    - sets up the initial state as $0,0,0,v,cte,e\psi$. The vehicle is at the origin of the vehicle coordinate system.
    - passes in the initial state and fitted coefficients (reference trajectory) to the solve() of MPC object, and receives the solver results
    - extracts actuator inputs from the solver and sends them to the simulator to actuate the vehicle
    - extracts the MPC predicted $x$ and $y$ coordinates and sends them to simulator for display
    - sends the waypoint vectors in vehicle coordinates to the simulator for display
  * polyfit() - finds the polynomial coefficients for the waypoints
  * polyval() - evaluates the polynomial fit using the fit coefficients at the input value
  * transform_to_vehicle_coord() - transforms the waypoints from map coordinates to vehicle coordinates using the delayed values of $x, y, \psi$.

* *MPC.cpp* - There are three key components
  * solve() 
      - takes as input the current state and the polynomial coefficients (reference trajectory) obtained by fitting to waypoints from simulator
      - bulk of this method is setting up the vehicle model constraints and variables for Ipopt solver
      - creates an instance of FG_eval class 
      - passes inputs variables (state variables and actuators) and constraints to the FG_eval class
      - passes in the FG_eval object, variables, and contraints to the Ipopt solver, and receives the predicted results
      - outputs the cost, the predicted results for the state variables for N time steps, and the predicted results for the actuators for the next time step to be sent to the simulator
  * global_kinematic() 
      - takes as input the current state, the actuator inputs, and the latency delay
      - calculates the next state vector using the bicycle motion model
      - outputs the next state vector
  * FG_eval class 
      - the class constructor takes in coefficients of the polynomial fitted to the waypoints obtained from the         simulator. These coefficients are used by the CTE and heading error equations
      - operator() method 
           - takes as input a vector containing the cost and the constraints (which is updated), and a vector containing the state variables and the actuators
           - defines the cost function and sets up the model constraints.  
  
The Term 2 simulator is a client, and the C++ program software is a web server. *main.cpp* is made up of several functions within main(), these all handle the uWebsocketIO communication between the simulator and it's self.

### Handling latency 

The latency of 100ms is modeled by a simple kinematic system (global_kinematic() of MPC object). We pass in the current state variables and actuator values received from the simulator, and the stipulated delay of 100ms to the kinematic system equations describing bicycle motion model that calculates and outputs the next state. This next state, transformed to vehicle coordinates, is the new initial state for the MPC.

### Transforming to vehicle coordinates 

The values of $x$, $y$, $\psi$, $v$, and the waypoints vectors received from simulator are in global map coordinate system. These values ($x$, $y$, $\psi$, $v$) along with the latency are passed through the global kinematic model to calculate the next state. The next state values of $x$, $y$, and $\psi$ are used to transform the waypoint vectors from global map coordinate system to vehicle coordinate system. First, each of the waypoints are adjusted by subtracting out $x$ and $y$ of the vehicle such that they are based on the vehicle's position. Next, the waypoint coordinates are changed using standard 2D vector transformation equations to be in vehicle coordinates:

$x_{waypoint} = x_{adjusted}.cos(-\psi) - y_{adjusted}.sin(-\psi)$

$y_{waypoint} = x_{adjusted}.sin(-\psi) + y_{adjusted}.cos(-\psi)$

Map coordinate to vehicle coordinate transformation is anticlockwise rotation by $\psi$ degrees. The vehicle coordinates assumes clockwise rotation. The transformation equations assume anticlockwise rotation. Hence, $\psi$ is replaced with $-\psi$ in the anticlockwise equations to get clockwise equations.

### Important note regarding steering angle $\delta$ 

If $\delta$ is positive we rotate counter-clockwise, or turn left.  In the simulator however, a positive value implies a right turn and a negative value implies a left turn. The update equations for orientation $\psi$ and orientation error e$\psi$ are updated as follows to account for this:

$\psi_{t+1} = \psi_{t} - \frac{v_{t}}{L_{f}}.\delta_{t}.dt$

$e\psi_{t+1} = e\psi_{t} - \frac{v_{t}}{L_{f}}.\delta_{t}.dt$



## Results 

The $v_{ref}$ was set to 80mph.

In all, there were ten hyperparameters - $N, dt$, and the scaling factors corresponding to eight different terms in the cost function.

With $N$ = 10 and $dt$ = 0.1, the car was not able to handle turns well due to less number of discrete points between subsequent states. Lower values of $dt$ result in oscillation of car at slow speeds. Higher values of $dt$ result in discretization issue. Large values of N result in slower implementation of the algorithm due to too many parameters. Any value of $N$ in the range of 7 to 10 and any value of $dt$ in the range of 0.05 to 0.08 such that the product $N*dt$ is in range $[0.49, 0.64]$ worked fine. 

For other eight hyperparameters, the following combinations of scaling factors seemed to work well.

|$cte$    |$e\psi$  |$v$      |$\delta$|$a$    | $v.\delta$ |$\delta_{diff}$|$a_{diff}$|    
|:-------:|:-------:|:-------:|:------:|:-----:|:----------:|:-------------:|:--------:|  
|  600    |  600    |  10     |  100   | 10    |  200       |  16           | 16       |  
|  1200   |  1200   |  10     |  100   | 10    |  400       |  32           | 32       |  
|  1500   |  1500   |  10     |  100   | 10    |  500       |  40           | 40       | 
|  1800   |  1800   |  10     |  100   | 10    |  600       |  48           | 48       | 
|  2000   |  2000   |  10     |  100   | 10    |  666       |  53           | 53       | 
|  3000   |  3000   |  10     |  100   | 10    |  1000      |  80           | 80       |  

With the above sets of hyperparameters, the car could run upto 80mph.


```python
# <video controls src="videos/MPC_demo.mp4" />
```

### **Acknowledgments** ###

I would like to thank Udacity for giving me this opportunity to work on an awesome project. I would like to offer thanks to Saurabh Sohoni as my approach to coordinate transformation, hyperparameter tuning, and latency handling were inspired by his work. Special thanks to the author of https://discussions.udacity.com/t/getting-started-with-docker-and-windows-for-the-ekf-project-a-guide/320236 which helped me setup the docker environment. 

